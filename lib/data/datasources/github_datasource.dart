import 'package:aqary/data/repositories/repository_repository.dart';
import 'package:flutter/foundation.dart'; // For kIsWeb check
import 'package:graphql_flutter/graphql_flutter.dart';

class GithubDatasource implements RepositoryRepository {
  static final HttpLink _httpLink = HttpLink(
    'https://api.github.com/graphql',
    defaultHeaders: {
      "Authorization":
          "Bearer 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}", // Placeholder for secure token retrieval
    },
  );

  final GraphQLClient _client = GraphQLClient(
    link: _httpLink,
    cache: GraphQLCache(store: InMemoryStore()),
  );

  final String readRepositories = """
    query TrendingRepos(\$language: String!) {
      search(query: "is:public sort:stars language:\$language", type: REPOSITORY, first: 50) {
        edges {
          node {
            ... on Repository {
              name
              description
              primaryLanguage {
                name
              }
              stargazers {
                totalCount
              }
              forks {
                totalCount
              }
            }
          }
        }
        pageInfo {
          hasNextPage
        }
      }
    }
  """;

  @override
  Future<List<Map<String, dynamic>>> getTrendingRepositories(
      String language) async {
    final result = await _client.query(
      QueryOptions(
        document: gql(readRepositories),
        variables: <String, dynamic>{
          'language': language,
        },
      ),
    );

    if (result.hasException) {
      throw Exception(result.exception);
    }

    return result.data!['search']['edges']?.cast<Map<String, dynamic>>();
  }

  // Placeholder for secure personal access token retrieval
  // **Important:** Do not store the token directly in the code.
  // Consider using a secure storage solution for native platforms
  // or environment variables for web platforms.
  String getPersonalAccessToken() {
    if (kIsWeb) {
      throw Exception(
          'Personal access tokens cannot be stored on web platforms due to security risks. Consider using environment variables.');
    } else {
      // Replace with your implementation for secure token retrieval
      // (e.g., secure storage on native platforms)
      throw Exception(
          'Personal access token retrieval not implemented. Please provide a secure method for obtaining the token.');
    }
  }
}
