import 'package:aqary/core/domain/entities/repository.dart';
import 'package:aqary/data/repositories/repository_repository.dart';

class GetTrendingRepositories {
  final RepositoryRepository repositoryRepository;

  GetTrendingRepositories(this.repositoryRepository);

  Future<List<Repository>> call(String language) async {
    final result = await repositoryRepository.getTrendingRepositories(language);
    return result.map((repo) => Repository.fromMap(repo)).toList();
  }
}
