class Repository {
  final String name;
  final String description;
  final String primaryLanguage;
  final int stargazersCount;
  final int forksCount;

  Repository({
    required this.name,
    required this.description,
    required this.primaryLanguage,
    required this.stargazersCount,
    required this.forksCount,
  });

  factory Repository.fromMap(Map<String, dynamic> map) {
    return Repository(
      name: map['name'] as String,
      description:
          map['description'] ?? '', // Handle potential missing description
      primaryLanguage: map['primaryLanguage']?['name'] ??
          '', // Handle potential missing language
      stargazersCount: map['stargazers']['totalCount'] as int,
      forksCount: map['forks']['totalCount'] as int,
    );
  }
}
