import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

void main() => runApp(MaterialApp(
    title: "Github with GraphQL",
    debugShowCheckedModeBanner: false,
    home: MyApp()));

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String personal_access_tokens = "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";

  @override
  Widget build(BuildContext context) {
    final HttpLink httpLink = HttpLink(
      'https://api.github.com/graphql',
      defaultHeaders: {"authorization": "Bearer $personal_access_tokens"},
    );

    ValueNotifier<GraphQLClient> client =
        ValueNotifier<GraphQLClient>(GraphQLClient(
      link: httpLink,
      cache: GraphQLCache(store: InMemoryStore()),
    ));

    return GraphQLProvider(
      client: client,
      child: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String selectedLanguage = 'Python';
  final List<String> languages = ['Python', 'JavaScript'];

  @override
  Widget build(BuildContext context) {
    String readRepositories = """
     query TrendingRepos() {
  search(query: "is:public sort:stars language:$selectedLanguage", type: REPOSITORY, first: 50) {
    edges {
      node {
        ... on Repository {
          name
          description
          primaryLanguage {
            name
          }
           stargazers {
            totalCount
          }
          forks {  
            totalCount
          }
        }
      }
    }
    pageInfo {
      hasNextPage
    }
  }

}
      """;

    return Scaffold(
      appBar: AppBar(
        title: Text("Github with GraphQL"),
        backgroundColor: Colors.black,
        centerTitle: true,
      ),
      body: Column(
        children: [
          DropdownButton<String>(
            value: selectedLanguage,
            onChanged: (String? newValue) {
              setState(() {
                selectedLanguage = newValue!;
              });
            },
            items: languages.map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
          ),
          Expanded(
            child: Query(
              options: QueryOptions(
                fetchPolicy: FetchPolicy
                    .cacheAndNetwork, // Optional: Configure fetch policy

                // Set a longer timeout (adjust as needed)
                pollInterval: Duration(seconds: 1000),
                document: gql(readRepositories),
              ),
              builder: (
                QueryResult result, {
                Future<QueryResult>? Function(FetchMoreOptions)? fetchMore,
                void Function()? refetch,
              }) {
                // if (result.hasException) {
                //   return Center(
                //     child: Text(
                //       result.exception.toString(),
                //       style: TextStyle(fontSize: 16),
                //       textAlign: TextAlign.center,
                //     ),
                //   );
                // }
                if (result.hasException) {
                  final exception = result.exception!;
                  String errorMessage = 'Error occurred';
                  if (exception.linkException != null) {
                    errorMessage = exception.linkException.toString();
                  } else if (exception.graphqlErrors.isNotEmpty) {
                    errorMessage = exception.graphqlErrors
                        .map((e) => e.toString())
                        .join('\n');
                  }
                  return Center(child: Text(errorMessage));
                }

                if (result.isLoading) {
                  return Center(child: CircularProgressIndicator());
                }

                final starredRepositories = result.data!['search']['edges'];

                return Padding(
                  padding: const EdgeInsets.only(top: 10.0),
                  child: ListView.builder(
                    itemCount: starredRepositories.length,
                    itemBuilder: (context, index) {
                      final repo = starredRepositories[index]['node'];
                      return ListTile(
                        title: Text(repo['name']),
                        subtitle: Text(repo['description'] ?? ''),
                        trailing:
                            Text('${repo['stargazers']['totalCount']} stars'),
                      );
                    },
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
